package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.AddProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.UpdateProductRequest
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.websocket.server.PathParam

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products")
    fun findProductsBySkus(@RequestParam("skus") skus:List<String> ) : ResponseEntity<List<ProductResponse>> {
        logger.info("Request for products with skus: $skus")

        val products = productService.findProductsBySkus(skus)

        return if(products.isEmpty()) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @PostMapping("/products")
    fun addProduct(@RequestBody productDto : AddProductRequest) : ResponseEntity<ProductResponse> {
        logger.info("Create product request with sku: ${productDto.sku}")

        val product = productService.createProduct(productDto.sku, productDto.name, productDto.description, productDto.price, productDto.stock)

        return ResponseEntity.ok(product);
    }

    @PatchMapping("/products/{sku}")
    fun updateProduct(@PathVariable("sku") sku:String, @RequestBody updateProductDto : UpdateProductRequest) : ResponseEntity<ProductResponse> {
        logger.info("Update product request with sku: $sku")

        val product = productService.updateProduct(sku,updateProductDto.name,updateProductDto.description,updateProductDto.price,updateProductDto.stock)

        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }

    }


}
