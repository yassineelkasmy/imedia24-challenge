package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.math.BigInteger
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        val product = productRepository.findBySku(sku)
        return product?.toProductResponse()
    }

    fun findProductsBySkus(skus:List<String>) :List<ProductResponse>{
        val products = productRepository.findBySkuIsIn(skus);

        return products.map { productEntity -> productEntity.toProductResponse() }
    }


    fun createProduct(sku:String, name:String, description:String?, price:BigDecimal, stock:Long?): ProductResponse {
        val now = ZonedDateTime.now()
        val product = ProductEntity(sku,name,description,price,stock,now,now)

        val savedProduct = productRepository.save(product)

        return savedProduct.toProductResponse();
    }


    fun updateProduct(sku: String, name: String?,description: String?,price: BigDecimal?,stock: Long?):ProductResponse? {
        val product = productRepository.findBySku(sku)
        if(product!=null) {
            val updatedProduct = ProductEntity(sku,
                name?:product.name,
                description?:product.description,
                price?:product.price,
                stock?:product.stock,
                product.createdAt,
                ZonedDateTime.now(),
                )

            return productRepository.save(updatedProduct).toProductResponse()
        }
        else {
            return null
        }

    }

}
