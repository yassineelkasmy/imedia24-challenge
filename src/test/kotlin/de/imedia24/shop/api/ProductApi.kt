package de.imedia24.shop.api

import de.imedia24.shop.controller.ProductController
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

import java.math.BigDecimal
import java.time.ZonedDateTime


@WebMvcTest(ProductController::class)
class ProductApi {

    @MockBean
    private lateinit var productService: ProductService

    @Autowired
    private lateinit var mockMvc : MockMvc


    @Test
    fun `Should Get Products By Skus`() {
        val now = ZonedDateTime.now()

        val product1 = ProductEntity("11","p1","d1", BigDecimal("10"),10, now, now)
        val product2 = ProductEntity("22","p2","d2", BigDecimal("10"),10, now, now)

        val expectedResponse = listOf(product1.toProductResponse(),product2.toProductResponse())

        `when`(productService.findProductsBySkus(anyList())).thenReturn(expectedResponse)

        mockMvc.perform(get("/products?skus=11,22"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.length()").value(expectedResponse.size))
            .andExpect(jsonPath("$[0].sku").value(expectedResponse[0].sku))
            .andExpect(jsonPath("$[1].sku").value(expectedResponse[1].sku))

    }

    @Test
    fun `Should Update a Product` () {
        val now = ZonedDateTime.now()
        val expectedResponse = ProductEntity("11","p11","d11", BigDecimal("15"),20, now, now).toProductResponse()

        `when`(productService.updateProduct(anyString(), anyString(), anyString(), any(), anyLong())).thenReturn(expectedResponse)
        mockMvc.perform(patch("/products/11").contentType("application/json").content("{\n" +
                "    \"name\":\"p11\",\n" +
                "    \"description\":\"d11\",\n" +
                "    \"price\":11,\n" +
                "    \"stock\":20\n" +
                "}"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.sku").value(expectedResponse.sku))
            .andExpect(jsonPath("$.name").value(expectedResponse.name))
            .andExpect(jsonPath("$.description").value(expectedResponse.description))
            .andExpect(jsonPath("$.price").value(expectedResponse.price))
            .andExpect(jsonPath("$.stock").value(expectedResponse.stock))



    }
}